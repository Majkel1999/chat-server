package main

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"net"
	"os"
	"sync"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/google/uuid"
)

type Message struct {
	Sender  string `json:"sender"`
	Message string `json:"message"`
}

func main() {
	redisClient := redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDIS_URL"),
		Password: "",
		DB:       0,
	})

	listener, err := net.Listen("tcp", ":9090")
	if err != nil {
		fmt.Printf("error starting server %s\n", err)
		return
	}
	fmt.Println("Server started")

	var connMap = &sync.Map{}
	redisSubscription := redisClient.Subscribe(context.Background(), "chat")
	go handleChatReceived(connMap, redisSubscription)

	defer func() {
		listener.Close()
		redisSubscription.Close()
	}()

	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Printf("error accepting connection %s\n", err)
			return
		}

		id := uuid.New().String()
		connMap.Store(id, conn)
		fmt.Printf("Client %s connected\n", id)
		go handleUserConnection(id, conn, connMap, redisClient)
	}
}

func handleChatReceived(connMap *sync.Map, sub *redis.PubSub) {
	ctx := context.Background()
	for {
		msg, err := sub.ReceiveMessage(ctx)
		if err != nil {
			fmt.Printf("error receiving message %s\n", err)
			time.Sleep(1 * time.Second)
			continue
		}

		connMap.Range(func(key, value interface{}) bool {
			if conn, ok := value.(net.Conn); ok {
				if _, err := conn.Write([]byte(msg.Payload + "\n")); err != nil {
					fmt.Printf("error sending message %s\n", err)
				}
			}
			return true
		})
	}
}

func handleUserConnection(id string, c net.Conn, connMap *sync.Map, rdb *redis.Client) {
	defer func() {
		c.Close()
		connMap.Delete(id)
	}()
	ctx := context.Background()
	for {
		userInput, err := bufio.NewReader(c).ReadString('\n')
		if err != nil {
			fmt.Printf("Client %s disconnected\n", id)
			return
		}
		msg := new(Message)
		msg.Sender = id
		msg.Message = userInput
		payload, err := json.Marshal(msg)
		if err != nil {
			fmt.Printf("error marshalling json %s\n", err)
			continue
		}
		err = rdb.Publish(ctx, "chat", payload).Err()
		if err != nil {
			fmt.Printf("error publishing message %s\n", err)
		}
	}
}
